# Alumni Network Frontend

##### Alumni Network - Live application : [![Live Application](https://img.shields.io/badge/-CLICK_HERE-1f425f.svg)](https://lucaspersson.gitlab.io/alumni-network)

A Vue application in JavaScript. Connected With Alumni Network Backend : [![BACKENDAPP](https://img.shields.io/badge/-CLICK_HERE-1f425f.svg)](https://gitlab.com/LucasPersson/alumni-network-backend)

![build](https://img.shields.io/badge/build-passing-green)

![keycloak](https://img.shields.io/badge/keycloak_version-^17.0.0-blue)
![vueversion](https://img.shields.io/badge/vue_version-^3.2.25-blue)
![vuex](https://img.shields.io/badge/vuex_version-^4.0.2-blue)
![vuerouter](https://img.shields.io/badge/vuerouter_version-^4.0.13-blue)
![vite](https://img.shields.io/badge/vite_version-^2.8.0-blue)
![tailwind](https://img.shields.io/badge/tailwind_version-^3.0.23-blue)
![bootstrap](https://img.shields.io/badge/bootstrap_version-^5.1.3-blue)
![axios](https://img.shields.io/badge/axios_version-^0.26.0-blue)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Testing](#testing)
- [Assumptions](#assumptions)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project is created for the purpose of creating a frontend application in Vue for our case Alumni Network.

## Install

```
-> Open Visual Studio Code
git clone https://gitlab.com/LucasPersson/alumni-network
cd alumni-network
npm install
```

## Usage

[![windows](https://img.shields.io/badge/--6fa8dc?logo=windows&logoColor=000)](https://www.microsoft.com/sv-se/windows)
[![npm](https://img.shields.io/badge/--ffffff?logo=npm&logoColor=000)](https://www.npmjs.com/)
[![jira](https://img.shields.io/badge/--005bf7?logo=jira&logoColor=000)](https://www.atlassian.com/software/jira)
[![JavaScript](https://img.shields.io/badge/--F7DF1E?logo=javascript&logoColor=000)](https://www.javascript.com/)
[![html](https://img.shields.io/badge/--f37711?logo=html5&logoColor=000)](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics)
[![tailwind](https://img.shields.io/badge/--a1caef?logo=tailwindcss&logoColor=000)](https://tailwindui.com/)
[![Visual Studio Code](https://img.shields.io/badge/--007ACC?logo=visual%20studio%20code&logoColor=ffffff)](https://code.visualstudio.com/)
[![vue](https://img.shields.io/badge/--459234?logo=vuedotjs&logoColor=000)](https://vuejs.org/)
[![swagger](https://img.shields.io/badge/--44d84a?logo=swagger&logoColor=000)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)
[![bootstrap](https://img.shields.io/badge/--6934ef?logo=bootstrap&logoColor=000)](https://getbootstrap.com/)
[![heroku](https://img.shields.io/badge/--9d1ef2?logo=heroku&logoColor=000)](https://alumni-be.herokuapp.com/)
[![keycloak](https://img.shields.io/badge/Keycloak-white.svg)](https://www.keycloak.org/)





```
-> Open Visual Studio Code
npm run dev
```


## Swagger

[![swagger](https://img.shields.io/badge/Open%20Swagger-HERE-green.svg)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)
```
-> Authorize

Username: swagger
Password: swagger

-> Access data
```

## Testing

For testing the endpoint of the application you can use the Swagger documentation UI [![swagger](https://img.shields.io/badge/-HERE-green.svg)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)

## User Manual

[![application](https://img.shields.io/badge/Link_to_User_Manual-HERE-orange.svg)](https://gitlab.com/LucasPersson/alumni-network/-/wikis/uploads/28d75182d5b2ee36183b3f524a1d6373/User_Manual_PDF.pdf)



## Assumptions

In the assignment we have made the following assumptions.

## Maintainers

[![GitLab](https://badgen.net/badge/icon/Lucas%20Persson?icon=gitlab&label)](https://gitlab.com/LucasPersson)
[![GitLab](https://badgen.net/badge/icon/Christian%20Neij?icon=gitlab&label)](https://gitlab.com/ChristianNeij)
[![GitLab](https://badgen.net/badge/icon/Zahra%20Ghadban?icon=gitlab&label)](https://gitlab.com/zizighadban)
[![GitLab](https://badgen.net/badge/icon/Anna%20Hallberg?icon=gitlab&label)](https://gitlab.com/haruberi)

## Mentor

[![GitLab](https://badgen.net/badge/icon/Nicholas%20Lennox?icon=gitlab&label)](https://gitlab.com/NicholasLennox)

## Contributing

PRs accepted.

* [![standard-readme compliant](https://img.shields.io/badge/standard_readme-HERE-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

* [![standard-readme compliant](https://img.shields.io/badge/readme_badges-HERE-green.svg?style=flat-square)](https://github.com/Naereen/badges/blob/master/README.md)

## License

MIT Licence © 2022 Lucas Persson, Christian Neij, Zahra Ghadban, Anna Hallberg

