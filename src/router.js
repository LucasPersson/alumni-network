import {createRouter, createWebHistory} from 'vue-router'
import groupDetail from './views/groups/GroupDetail.vue'
import groupList from './views/groups/GroupList.vue'
import timeLine from './views/post/TimeLine.vue'
import viewThread from './views/post/ViewThread.vue'
import topicList from './views/topic/TopicList.vue'
import topicDetail from './views/topic/TopicDetail.vue'
import userDashboard from './views/user/UserDashboard.vue'
import userDetail from './views/user/UserDetail.vue'
import userSettings from './views/user/UserSettings.vue'
import errorPage from './views/errorPage.vue'
import store from './store'

const authGuard = (to, from, next) => {
    if(!store.state.user.hasOwnProperty('status') && (store.state.user.hasOwnProperty('id'))){
        next('/userSettings')
    }else {
        next()
    }
}

const routes = [
    {
        path: '/',
        component: timeLine,
        beforeEnter: authGuard
    },
    {
        path: '/groupDetail/:id',
        component: groupDetail,
        beforeEnter: authGuard
    },
    {
        path: '/groups',
        component: groupList,
        beforeEnter: authGuard
    },
    {
        path: '/thread/:id',
        component: viewThread,
        beforeEnter: authGuard
    },
    {
        path: '/topics',
        component: topicList,
        beforeEnter: authGuard
    },
    {
        path: '/topicDetail/:id',
        component: topicDetail,
        beforeEnter: authGuard
    },
    {
        path: '/dashboard',
        component: userDashboard,
        beforeEnter: authGuard
    },
    {
        path: '/userDetail/:id',
        component: userDetail,
        beforeEnter: authGuard
    },
    {
        path: '/userSettings',
        component: userSettings
    },
    {
        path: '/404',
        component: errorPage
    },
    {
        path: '/:catchAll(.*)',
        redirect: '/404'
    }
]

export default createRouter({
    history: createWebHistory(
      process.env.NODE_ENV === 'production'
        ? '/alumni-network'
        : '/'
    ),
    routes,
  })
