import { createApp } from 'vue'
import store from './store'
import App from './App.vue'
import router from './router'
import './index.css'
import Keycloak from 'keycloak-js'
import axiosBackend from 'axios'
import { getCurrentAlumniUser } from './api/alumniUserApi'



const keycloak = Keycloak( process.env.NODE_ENV === 'production' ? 
{
  realm : 'myrealm',
  url : 'https://alumni-network-keycloak.herokuapp.com/auth/',
  clientId : 'Vue-app-production',
 
} : '../keycloak.json')

export const logoutKeycloak = keycloak.logout

async function authenticateAgainstKeycloak () {

    await keycloak.init({ onLoad: 'login-required' }).then((auth) => {
      if (!auth) {
        window.location.reload()
      } else {
        console.log('Authenticated')
      }
      
      if (keycloak.token) {
        window.localStorage.setItem('keycloakToken', keycloak.token)
    
      }
    })
    
    const currentUser = await getCurrentAlumniUser()
    if (currentUser[1] === 200) {
      await router.push('/')

    } else if (currentUser[1] === 201) {
      await router.push('/userSettings')

    } else {
      await router.push('/404')
      
    }
}

axiosBackend.interceptors.response.use(function (response) {
  return response
},async function (error) {
  console.log(error)
  if (error.response.status === 403) {
    await authenticateAgainstKeycloak()
    return error
  }
  return error
})

function instantiateVueApp () {
  createApp(App)
    .use(router)
    .use(store)
    .mount('#app')
}
  
if (!localStorage.getItem('keycloakToken')) {
  authenticateAgainstKeycloak().then(() => {
    instantiateVueApp()
  })
} else {
  instantiateVueApp()
}

  