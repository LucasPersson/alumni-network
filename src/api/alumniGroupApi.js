import { BASE_URL, getToken } from "./apiVariables";

export async function getAllAlumniGroups() {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/alumniGroup/all`, config
      );
      const { data } = await response.json().then((data) => {
    
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}


export async function getAlumniGroupById(id) {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/alumniGroup/${id}`, config
      );
      const { data } = await response.json().then((data) => {
        
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}


export async function joinAlumniGroup(groupId, userId) { //Joins userId and groupId in a many-to-many table.
  try {
    const config = {
        
      method: "POST",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: userId
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/alumniGroup/${groupId}/join`, config
    );
    const { data } = await response.json().then((data) => {

      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function createAlumniGroup(group) {
  try {
    const config = {
        
      method: "POST",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: JSON.stringify(group)
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/alumniGroup`, config
    );
    const { data } = await response.json().then((data) => {
    
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}