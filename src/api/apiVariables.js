export const BASE_URL = process.env.NODE_ENV === 'production' ? "https://alumni-be.herokuapp.com" : "http://localhost:8080";


export const getToken = () => {
    
    const jwt = localStorage.getItem('keycloakToken')
    return `Bearer ${jwt}`
}
