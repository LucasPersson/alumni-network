import { BASE_URL, getToken } from "./apiVariables";

export async function getAllTopics() {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/topic/all`, config
      );
      const { data } = await response.json().then((data) => {
      
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}


export async function getTopicById(id) {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/topic/${id}`, config
      );
      const { data } = await response.json().then((data) => {
   
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}


export async function joinTopic(topicId, userId) { //Joins userId and topicId in a many-to-many table.
  try {
    const config = {
        
      method: "POST",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: userId
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/topic/${topicId}/join`, config
    );
    const { data } = await response.json().then((data) => {
     
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}

export async function createTopic(topic) {
  try {
    const config = {
        
      method: "POST",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: JSON.stringify(topic)
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/topic`, config
    );
    const { data } = await response.json().then((data) => {
      
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}