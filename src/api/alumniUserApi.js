import { BASE_URL, getToken } from "./apiVariables";

export async function getCurrentAlumniUser() {
  try {
    const config = {
        
      method: "GET",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      }
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/alumniUser`, config
    );
    const { data } = await response.json().then((data) => {
  
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function getAlumniUserById(id) {
  try {
    const config = {
        
      method: "GET",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      }
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/alumniUser/${id}`, config
    );
    const { data } = await response.json().then((data) => {
    
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function updateAlumniUser(alumniUserId, alumniUser) {
  try {
    const config = {
        
      method: "PATCH",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: JSON.stringify(alumniUser)
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/alumniUser/${alumniUserId}`, config
    );
    const { data } = await response.json().then((data) => {
    
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}