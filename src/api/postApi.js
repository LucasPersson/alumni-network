import { BASE_URL, getToken } from "./apiVariables";

export async function getAllPosts() {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/post/all`, config
      );
      const { data } = await response.json().then((data) => {
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}

export async function getPostsById(id) { //Fetches post identified by id along with the replies to that post.
  try {
    const config = {
        
      method: "GET",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      }
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/post/${id}`, config
    );
    const { data } = await response.json().then((data) => {
   
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function getPostsByAlumniGroupId(id) {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/post/alumniGroup/${id}`, config
      );
      const { data } = await response.json().then((data) => {
     
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}

export async function getPostsByAlumniUserId(id) { //This api-request and end-point are not being used for this version of the product.
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/post/alumniUser/${id}`, config
      );
      const { data } = await response.json().then((data) => {
    
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}

export async function getPostsDirectedToUserWithId(id){ //Function fetches all posts that have the user identified by the id as the post's intended recipient with the targetUser field as well as all replies in the threads created by posts to the user.
  try {
    const config = {
        
      method: "GET",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      }
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/post/targetUser/${id}`, config
    );
    const { data } = await response.json().then((data) => {
  
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function getPostsByTopicId(id) {
    try {
      const config = {
          
        method: "GET",
        headers : {
          "Access-Control-Allow-Origin" : "*",
          "Content-Type": "application/json",
          "Authorization" : getToken()
        }
      };
  
      const response = await fetch(
        `${BASE_URL}/api/v1/post/topic/${id}`, config
      );
      const { data } = await response.json().then((data) => {
   
        return data;
      });
      return [data, response.status];
    } catch (error) {
      return [error.message];
    }
}


export async function createPost(post) {
  try {
    const config = {
        
      method: "POST",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: JSON.stringify(post)
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/post`, config
    );
    const { data } = await response.json().then((data) => {
    
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}


export async function updatePost(postId, post) {
  try {
    const config = {
        
      method: "PUT",
      headers : {
        "Access-Control-Allow-Origin" : "*",
        "Content-Type": "application/json",
        "Authorization" : getToken()
      },
      body: JSON.stringify(post)
    };

    const response = await fetch(
      `${BASE_URL}/api/v1/post/${postId}`, config
    );
    const { data } = await response.json().then((data) => {
    
      return data;
    });
    return [data, response.status];
  } catch (error) {
    return [error.message];
  }
}