import {createStore} from 'vuex'
import { getAllAlumniGroups, getAlumniGroupById, createAlumniGroup, joinAlumniGroup } from './api/alumniGroupApi'
import { getAlumniUserById, getCurrentAlumniUser, updateAlumniUser } from './api/alumniUserApi'
import { createPost, getAllPosts, getPostsByAlumniGroupId, getPostsByAlumniUserId, getPostsById, getPostsByTopicId, getPostsDirectedToUserWithId, updatePost } from './api/postApi'
import { getAllTopics, getTopicById, createTopic, joinTopic } from './api/topicApi'


//Function to every time you refresh, you don't have to log in again
const initUser = () => {
    const storedUser = localStorage.getItem('user')
    if(!storedUser){
        return null
    }
    return JSON.parse(storedUser)
}

//Global state
export default createStore({
    state: {
        authenticated: false,
        user: {},
        searchText: '',
        posts: [],
        users: [],
        groups: [],
        topics: []
    },
    getters : {
        replyPostMatchesSearchText: (state) => (post) => { //Function that accepts a post and then searches for replies to that post among posts in state and checks if reply-post has a match with search-criteria in searchText.
            let childMatchExists = false
            for (const childPost of state.posts) {
                if(childPost.hasOwnProperty('replyParentId') && childPost.replyParentId === post.id){
                    childMatchExists = childPost.content.toLowerCase().includes(state.searchText.toLowerCase())
                    if(childMatchExists){
                        break
                    }
                }
            }
            return childMatchExists
        },
        filteredPostsBySearchText: (state, getters) => { //A function that returns posts that have a match with search-criteria in searchText. If either title or content matches the post will be returned. Function also filters out posts that otherwise aren't meant for the user's eyes. Also calls on method "replyPostMatchesSearchText" to compare replies to a post with searchText.
            return state.posts.filter((post) => {
                if(post.hasOwnProperty('replyParentId')){
                    return false
                }
                else if(post.hasOwnProperty('alumniGroup') && !state.user.alumniGroups.includes(post.alumniGroup)){
                    return false
                }
                else if(post.hasOwnProperty('topic') && !state.user.topics.includes(post.topic)){
                    return false
                }
                else if(post.hasOwnProperty('targetUser')){
                    return false
                }
                else if(!post.content.toLowerCase().includes(state.searchText.toLowerCase()) && !post.title.toLowerCase().includes(state.searchText.toLowerCase())){
                    return getters.replyPostMatchesSearchText(post)
                }else{
                    return true
                }
            })
        },
        filteredPreFilteredPosts: (state, getters) => { //A filter function similar to filteredPostsBySearchText but only filters so replies aren't shown while still comparing replies with searchText. This function assumes that fetched posts are pre-filtered (by the API-GET used) to keep unrelated posts away from the array. Used by dashboard, topic detail and group detail.
            return state.posts.filter((post) => {
                if(post.hasOwnProperty('replyParentId')){
                    return false
                }
                else if(!post.content.toLowerCase().includes(state.searchText.toLowerCase()) && !post.title.toLowerCase().includes(state.searchText.toLowerCase())){
                    return getters.replyPostMatchesSearchText(post)
                }else{
                    return true
                }
            })
        },
        filteredGroups: (state) => { //Filters groups to hide private groups that the user is not a member of from the user.
            return state.groups.filter((group) => {
                if(group.isPrivate && !state.user.alumniGroups.includes(group.id)){
                    return false
                }
                else{           
                    return true
                }
            })
        },
        getTopics: (state) => {
            return state.topics
        },
        getAllPosts: (state) => {
            return state.posts
        },
        getModifiedProperties: (state) => (object) => { //A function that takes in objects that may contain references to other objects, for example a Post that was created by a user. The references to other objects is received as strings that are undesirable in the FE-part of the application. This function removes the unnecessary string parts of the object reference and parses the id part into a number property that is saved to the parameter object. If the string properties are empty they are removed.
            if(object.hasOwnProperty('alumniGroup')){
                if(object.alumniGroup.length === 0){
                    const {alumniGroup, ...restObject} = object
                    object = restObject
                }else{
                    const stringArray = object.alumniGroup.split("/")
                    object.alumniGroup = parseInt(stringArray[stringArray.length-1])
                }
            }
            if(object.hasOwnProperty('alumniGroups')){
                const newArray = []
                for (const group of object.alumniGroups) {
                    const stringArray = group.split("/")
                    newArray.push(parseInt(stringArray[stringArray.length-1]))
                }
                object.alumniGroups = newArray
            }
            if(object.hasOwnProperty('alumniUser')){
                if(object.alumniUser.length === 0){
                    const {alumniUser, ...restObject} = object
                    object = restObject
                }else{
                    const stringArray = object.alumniUser.split("/")
                    object.alumniUser = parseInt(stringArray[stringArray.length-1])
                }
            }
            if(object.hasOwnProperty('alumniUsers')){
                const newArray = []
                for (const user of object.alumniUsers) {
                    const stringArray = user.split("/")
                    newArray.push(parseInt(stringArray[stringArray.length-1]))
                }
                object.alumniUsers = newArray
            }
            if(object.hasOwnProperty('topic')){
                if(object.topic.length === 0){
                    const {topic, ...restObject} = object
                    object = restObject
                }else{
                    const stringArray = object.topic.split('/')
                    object.topic = parseInt(stringArray[stringArray.length-1])
                }
            }
            if(object.hasOwnProperty('topics')){
                const newArray = []
                for (const topic of object.topics) {
                    const stringArray = topic.split('/')
                    newArray.push(parseInt(stringArray[stringArray.length-1]))
                }
                object.topics = newArray
            }
            if(object.hasOwnProperty('posts')){
                const newArray = []
                for (const post of object.posts) {
                    const stringArray = post.split('/')
                    newArray.push(parseInt(stringArray[stringArray.length-1]))
                }
                object.posts = newArray
            }
            return object
        },
        findPostOrder: (state) => (postArray) => { //Function receives a sorted array or posts that is sorted by date but doesn't account for whether the post is an original post or a reply. This function goes through the array and creates a new array that only stores id's of original posts but also checks the replyParentId property of replies so old original posts with new replies will be stored high (unless called by action that stores earliest posts first in which case it's the opposite). A map with the original posts is also created and both the map and the id-array are returned. 
            const postMap = new Map()
            const postOrder = []
            for(const post of postArray){
                if(post.hasOwnProperty('replyParentId')){
                    if(!postOrder.includes(post.replyParentId)){
                        postOrder.push(post.replyParentId)
                    }
                }else{
                    postMap.set(post.id, post)
                    if(!postOrder.includes(post.id)){
                        postOrder.push(post.id)
                    }
                }
            }
            return [postOrder, postMap]
        }
    },
    mutations: {
        SET_AUTH: (state, authenticated) => state.authenticated = authenticated,
        setUser: (state, user) =>{
            if(!user.hasOwnProperty('alumniGroups')){
                user.alumniGroups = []
            }
            if(!user.hasOwnProperty('topics')){
                user.topics = []
            }
            if(!user.hasOwnProperty('posts')){
                user.posts = []
            }
            state.user = user
        },
        clearUser: (state) => {
            state.user = {}
        },
        setSearchText: (state, text) => {
            state.searchText = text
        },
        storeUser: (state, user) => {
            state.users.push(user)
        },
        clearUsers: (state) => {
            state.users = []
        },
        addUserGroup: (state, group) => {
            state.user.alumniGroups.push(group)
        },
        addUserTopic: (state, topic) => {
            state.user.topics.push(topic)
        },
        storeTopics: (state, topics) => {
            state.topics = topics
        },
        storeTopic: (state, topic) => {
            state.topics.push(topic)
        },
        clearTopics: (state) => {
            state.topics = []
        },
        storeGroups: (state, groups) => {
            state.groups = groups
        },
        storeGroup: (state, group) => {
            state.groups.push(group)
        },
        clearGroups: (state) => {
            state.groups = []
        },
        storePosts: (state, posts) => {
            state.posts = posts
        },
        storePost: (state, post) => {
            state.posts.push(post)
        },
        clearPosts: (state) => {
            state.posts = [];
        },
        updatePost: (state, updatedPost) => {
            for(const post of state.posts){
                if(post.id === updatedPost.id){
                    post.content = updatedPost.content
                    break
                }
            }
        }
    },
    actions: {
        setAuth: ({commit}, authenticated) => commit('SET_AUTH', authenticated),
        async changeUserDetails({state, commit, getters}, {userChanges}){ //Updates fields in user object, can only be done to logged in user, updates API and locally.
            const response = await updateAlumniUser(state.user.id, userChanges)
            if(response[1] === 200 || response[1] === 201){
                const modifiedUser = getters.getModifiedProperties(response[0])
                commit('setUser', modifiedUser)
            }
            else{
                return null
            }
        },
        async fetchCurrentUser({commit, getters}){ //Fetches the logged in user from API.
            const response = await getCurrentAlumniUser()
            if(response[1] === 200 || response[1] === 201){
                const modifiedUser = getters.getModifiedProperties(response[0])
                commit('setUser', modifiedUser)
            }
            else{
                return null
            }
        },
        async fetchUserById({commit, getters}, {id}){
            const response = await getAlumniUserById(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedUser = getters.getModifiedProperties(response[0])
                commit("storeUser", modifiedUser)
            }
            else{
                return null
            }
        },
        async fetchAllGroups({commit, getters}){
            const response = await getAllAlumniGroups()
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storeGroups', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchGroupById({commit, getters}, {id}){
            const response = await getAlumniGroupById(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedGroup = getters.getModifiedProperties(response[0])
                commit('storeGroup', modifiedGroup)
            }
            else{
                return null
            }
        },
        async fetchAllPosts({commit, getters}){
            const response = await getAllPosts()
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchPostsByGroupId({commit, getters}, {id}){
            const response = await getPostsByAlumniGroupId(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchPostsByUserId({commit, getters}, {id}){ //This action and its corresponding api-request and end-point are not being used for this version of the product.
            const response = await getPostsByAlumniUserId(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchPostsByTopicId({commit, getters}, {id}){
            const response = await getPostsByTopicId(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchAllTopics({commit, getters}){
            const response = await getAllTopics()
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storeTopics', modifiedArray)
            }
            else{
                return null
            }
        },
        async fetchTopicById({commit, getters}, {id}){
            const response = await getTopicById(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedGroup = getters.getModifiedProperties(response[0])
                commit('storeTopic', modifiedGroup)
            }
            else{
                return null
            }
        },
        sortPostsLatestFirst({state, commit, getters}){ //A function that sorts all fetched posts by timestamp placing latest post first and earliest post last. Not quite that straight forward though, it's used where replies are not supposed to be shown in the sorted list but still affect it. Replies to original posts "push up" the original posts in the sorting without being shown themselves.
            const currentPosts = state.posts
            currentPosts.sort((post1, post2) => {
                const date1 = new Date(post1.timestamp)
                const date2 = new Date(post2.timestamp)
                return (-1)*(date1 - date2)
            })
            const response = getters.findPostOrder(currentPosts)
            const postOrder = response[0]
            const postMap = response[1]
            const parentPostList = []
            for(const id of postOrder){
                parentPostList.push(postMap.get(id))
            }
            const childPosts = currentPosts.filter(post => {
                return post.hasOwnProperty('replyParentId')
            })
            const resultingArray = parentPostList.concat(childPosts)
            commit('storePosts', resultingArray)
        },
        sortPostsEarliestFirst({state, commit, getters}){ //Same as the other sorting function above but shows earliest posts first instead of last.
            const currentPosts = state.posts
            currentPosts.sort((post1, post2) => {
                const date1 = new Date(post1.timestamp)
                const date2 = new Date(post2.timestamp)
                return date1 - date2
            })
            const response = getters.findPostOrder(currentPosts)
            const postOrder = response[0]
            const postMap = response[1]
            const parentPostList = []
            for(const id of postOrder){
                parentPostList.push(postMap.get(id))
            }
            const childPosts = currentPosts.filter(post => {
                return post.hasOwnProperty('replyParentId')
            })
            const resultingArray = parentPostList.concat(childPosts)
            commit('storePosts', resultingArray)
        },
        async fetchPostAndRepliesById({commit, getters}, {id}){ //Fetches the original post identified by the id and also fetches the replies to that post and puts them all in a list together.
            const response = await getPostsById(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async createPost({commit, getters}, {post}){
            const response = await createPost(post)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = getters.getModifiedProperties(response[0])
                commit('storePost', modifiedArray)
            }
            else{
                return null
            }
        },
        async getPostsDirectedToUserWithId({commit, getters}, {id}){ //Function fetches all posts that have the user identified by the id as the post's intended recipient with the targetUser field as well as all replies in the threads created by posts to the user.
            const response = await getPostsDirectedToUserWithId(id)
            if(response[1] === 200 || response[1] === 201){
                const modifiedArray = response[0].map(post => {
                    return getters.getModifiedProperties(post)
                })
                commit('storePosts', modifiedArray)
            }
            else{
                return null
            }
        },
        async editPost({commit, getters}, {id, post}){ //Edits a post in the API and locally.
            const response = await updatePost(id, post)
            if(response[1] === 200 || response[1] === 201){
                const modifiedPost = getters.getModifiedProperties(response[0])
                commit('updatePost', modifiedPost)
            }
            else{
                return null
            }
        },
        async createTopic({state, commit, getters}, {newTopic}){
            const response = await createTopic(newTopic)
            if(response[1] === 200 || response[1] === 201){
                const modifiedTopic = getters.getModifiedProperties(response[0])
                commit('storeTopic', modifiedTopic)
                const topicId = modifiedTopic.id
                const userId = state.user.id
                await joinTopic(topicId, userId)
                commit('addUserTopic', topicId)
                return topicId
            }
            else{
                return null
            }
        },
        async createAlumniGroup({state, commit, getters}, {newGroup}){
            const response = await createAlumniGroup(newGroup)
            if(response[1] === 200 || response[1] === 201){
                const modifiedGroup = getters.getModifiedProperties(response[0])
                commit('storeGroup', modifiedGroup)
                const groupId = modifiedGroup.id
                const userId = state.user.id
                await joinAlumniGroup(groupId, userId)
                commit('addUserGroup', groupId)
                return groupId
            }
            else{
                return null
            }
        },
        async joinGroup({commit, getters}, {groupId, userId}){
            const response = await joinAlumniGroup(groupId, userId)
            if(response[1] === 200 || response[1] === 201){
                const modifiedUser = getters.getModifiedProperties(response[0])
                commit('setUser', modifiedUser)
            }
            else{
                return null
            }
        },
        async joinTopic({commit, getters}, {topicId, userId}){
            const response = await joinTopic(topicId, userId)
            if(response[1] === 200 || response[1] === 201){
                const modifiedUser = getters.getModifiedProperties(response[0])
                commit('setUser', modifiedUser)
            }
            else{
                return null
            }
        }
    }
})